﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task3_Entity;
using Task3_Entity.Models;

namespace StudentsRating.Services
{
    public class StudentServise
    {
        private readonly StudentContext studentContext;

        public StudentServise(StudentContext studentContext)
        {
            this.studentContext = studentContext;
        }

        public float GetAvrRaitAll()
        {
            var avr = studentContext.Raitings.Average(s => s.Raiting);
            return avr;
        }

        public float GetAvrRaitForClasses(string ClassName)
        {
            if (ClassName != null)
            {
                var avgc = studentContext.Raitings.Where(X => X.Students.Classes.Name == ClassName).Average(s => s.Raiting);
                return avgc;
            }
            else throw new ArgumentNullException();
        }

        public float GetAvrRaitForTask(string TaskName)
        {
            if (TaskName != null)
            {
                var avrt = studentContext.Raitings.Where(x => x.TaskName == TaskName).Average(s => s.Raiting);
                return avrt;
            }
            else throw new ArgumentNullException();
        }

        public List<string> GetTopThreeRaitEachClass()
        {
            List<string> keyValuePairs = new List<string>();
            int i = 1;
            for (; i <= studentContext.Classes.Count(); i++)
            {
                string tmp=studentContext.Classes.Where(x=>x.Id==i).Select(x=>x.Name).Single()+" class with top raiting:";
                var k=(studentContext.Raitings.Where(x => x.Students.Classes_Id == i).OrderByDescending(x => x.Raiting).Select(x => x.Raiting).Take(3).ToList());
                foreach(var item in k) { tmp += item.ToString() + " "; }
                keyValuePairs.Add(tmp);
            }
            return keyValuePairs;
        }

        public List<string> GetThreeWorstStudents()
        {
            List<string> keyValuePairs = new List<string>();
            keyValuePairs = studentContext.Raitings.OrderBy(x => x.Raiting).Select(x => x.Students.FirstName + " " + x.Students.LastName).Take(3).ToList();
            return keyValuePairs;
        }
    }
}
