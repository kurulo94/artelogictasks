﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity.Core.Repositories;

namespace Task3_Entity.Core
{
    public interface IUnitOfWork:IDisposable
    {
        IClassRepository Classes { get; }
        IStudentRepository Students { get; }
        IRaitingRepository Raitings { get; }
        int Complete();
    }
}
