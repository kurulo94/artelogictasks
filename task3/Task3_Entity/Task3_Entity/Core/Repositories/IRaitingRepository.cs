﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity.Models;

namespace Task3_Entity.Core.Repositories
{
    public interface IRaitingRepository : IRepository<Raitings>
    {
        float GetAvrRaitAll();
        float GetAvrRaitForClasses(string ClassName);
        float GetAvrRaitForTask(string TaskName);
    }
}
