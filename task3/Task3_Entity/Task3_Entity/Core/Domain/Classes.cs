﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Entity.Models
{
    public class Classes
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Classes()
        {
            Students = new HashSet<Students>();
        }

        public virtual ICollection<Students> Students { get; set; }

        public override bool Equals(object obj)
        {
            Classes classes = obj as Classes;
            if ((object)classes == null) { return false; }
            return base.Equals(obj);
        }
        public  bool Equals(Classes c)
        {
            return base.Equals((Classes)c);
        }
    }
}
