﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Entity.Models
{
    public class Students
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Classes_Id { get; set; }

        public Students()
        {
            Raitings = new HashSet<Raitings>();
        }

        public virtual ICollection<Raitings> Raitings { get; set; }  
        public virtual Classes Classes { get; set; }
        public override bool Equals(object obj)
        {
            Students classes = obj as Students;
            if ((object)classes == null) { return false; }
            return base.Equals(obj);
        }
        public bool Equals(Students c)
        {
            return base.Equals((Students)c);
        }
    }
}
