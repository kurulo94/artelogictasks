﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Entity.Models
{
   public class Raitings
    {
        public int Id { get; set; }
        public int Students_Id { get; set; }
        public string TaskName { get; set; }
        public float Raiting { get; set; }

        public virtual Students Students { get; set; }
        public override bool Equals(object obj)
        {
            Raitings classes = obj as Raitings;
            if ((object)classes == null) { return false; }
            return base.Equals(obj);
        }
        public bool Equals(Raitings c)
        {
            return base.Equals((Raitings)c);
        }
    }
}
