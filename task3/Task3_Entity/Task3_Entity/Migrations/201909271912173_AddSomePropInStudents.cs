﻿namespace Task3_Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSomePropInStudents : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "test", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "test");
        }
    }
}
