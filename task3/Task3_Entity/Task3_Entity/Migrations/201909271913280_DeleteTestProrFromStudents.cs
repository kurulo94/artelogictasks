﻿namespace Task3_Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteTestProrFromStudents : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Students", "test");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Students", "test", c => c.Int(nullable: false));
        }
    }
}
