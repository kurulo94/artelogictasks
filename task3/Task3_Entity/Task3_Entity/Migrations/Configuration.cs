﻿namespace Task3_Entity.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Task3_Entity.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<StudentContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Task3_Entity.StudentContext";
        }

        protected override void Seed(StudentContext studentContext)
        {
            var classlist = new List<Classes>()
            {
                new Classes() { Id = 1, Name = "1b" },
                new Classes() { Id = 2, Name = "2b"}
            };
            classlist.ForEach(s => studentContext.Classes.Add(s));
            studentContext.SaveChanges();

            var studentlist = new List<Students>()
            {
                new Models.Students { Id = 1, Classes_Id = 1, FirstName = "Roman", LastName = "Kyryliuk" },
                new Models.Students { Id = 2, Classes_Id = 1, FirstName = "Ivan", LastName = "Vinuk" },
                new Models.Students { Id = 3, Classes_Id = 1, FirstName = "Petro", LastName = "Kaban" },
                new Models.Students { Id = 4, Classes_Id = 1, FirstName = "Olexa", LastName = "Dron" },
                new Models.Students { Id = 5, Classes_Id = 1, FirstName = "Alex", LastName = "Puh" },
                new Models.Students { Id = 6, Classes_Id = 1, FirstName = "Viktor", LastName = "Senica" },
                new Models.Students { Id = 7, Classes_Id = 2, FirstName = "Vitas", LastName = "Mask" },
                new Models.Students { Id = 8, Classes_Id = 2, FirstName = "Orest", LastName = "Lolim" },
                new Models.Students { Id = 9, Classes_Id = 2, FirstName = "Illia", LastName = "Mazur" },
                new Models.Students { Id = 10, Classes_Id = 2, FirstName = "Valerian", LastName = "Gnatiyk" },
                new Models.Students { Id = 11, Classes_Id = 2, FirstName = "Bogdan", LastName = "Fediyk" },
                new Models.Students { Id = 12, Classes_Id = 2, FirstName = "Fedor", LastName = "Diduk" }
            };
            studentlist.ForEach(s => studentContext.Students.Add(s));
            studentContext.SaveChanges();

            var raitinglist = new List<Raitings>()
            {
                new Models.Raitings { Id = 1, Students_Id = 1, Raiting = 4, TaskName = "Task1" },
                new Models.Raitings { Id = 2, Students_Id = 2, Raiting = 10, TaskName = "Task1" },
                new Models.Raitings { Id = 3, Students_Id = 3, Raiting = 1, TaskName = "Task1" },
                new Models.Raitings { Id = 4, Students_Id = 4, Raiting = 2, TaskName = "Task1" },
                new Models.Raitings { Id = 5, Students_Id = 5, Raiting = 6, TaskName = "Task1" },
                new Models.Raitings { Id = 6, Students_Id = 6, Raiting = 7, TaskName = "Task1" },
                new Models.Raitings { Id = 7, Students_Id = 7, Raiting = 9, TaskName = "Task1" },
                new Models.Raitings { Id = 8, Students_Id = 8, Raiting = 10, TaskName = "Task1" },
                new Models.Raitings { Id = 9, Students_Id = 9, Raiting = 12, TaskName = "Task1" },
                new Models.Raitings { Id = 10, Students_Id = 10, Raiting = 1, TaskName = "Task1" },
                new Models.Raitings { Id = 11, Students_Id = 11, Raiting = 3, TaskName = "Task1" },
                new Models.Raitings { Id = 12, Students_Id = 12, Raiting = 5, TaskName = "Task1" },
                new Models.Raitings { Id = 13, Students_Id = 1, Raiting = 2, TaskName = "Task2" },
                new Models.Raitings { Id = 14, Students_Id = 2, Raiting = 11, TaskName = "Task2" },
                new Models.Raitings { Id = 15, Students_Id = 3, Raiting = 11, TaskName = "Task2" },
                new Models.Raitings { Id = 16, Students_Id = 4, Raiting = 12, TaskName = "Task2" },
                new Models.Raitings { Id = 17, Students_Id = 5, Raiting = 3, TaskName = "Task2" },
                new Models.Raitings { Id = 18, Students_Id = 6, Raiting = 4, TaskName = "Task2" },
                new Models.Raitings { Id = 19, Students_Id = 7, Raiting = 5, TaskName = "Task2" },
                new Models.Raitings { Id = 20, Students_Id = 8, Raiting = 11, TaskName = "Task2" },
                new Models.Raitings { Id = 21, Students_Id = 9, Raiting = 10, TaskName = "Task2" },
                new Models.Raitings { Id = 22, Students_Id = 10, Raiting = 11, TaskName = "Task2" },
                new Models.Raitings { Id = 23, Students_Id = 11, Raiting =5, TaskName = "Task2" },
                new Models.Raitings { Id = 24, Students_Id = 12, Raiting = 6, TaskName = "Task2" },
                new Models.Raitings { Id = 25, Students_Id = 1, Raiting = 9, TaskName = "Task3" },
                new Models.Raitings { Id = 26, Students_Id = 2, Raiting = 11, TaskName = "Task3" },
                new Models.Raitings { Id = 27, Students_Id = 3, Raiting = 12, TaskName = "Task3" },
                new Models.Raitings { Id = 28, Students_Id = 4, Raiting = 3, TaskName = "Task3" },
                new Models.Raitings { Id = 29, Students_Id = 5, Raiting = 5, TaskName = "Task3" },
                new Models.Raitings { Id = 30, Students_Id = 6, Raiting = 8, TaskName = "Task3" },
                new Models.Raitings { Id = 31, Students_Id = 7, Raiting = 1, TaskName = "Task3" },
                new Models.Raitings { Id = 32, Students_Id = 8, Raiting = 1, TaskName = "Task3" },
                new Models.Raitings { Id = 33, Students_Id = 9, Raiting = 11, TaskName = "Task3" },
                new Models.Raitings { Id = 34, Students_Id = 10, Raiting = 6, TaskName = "Task3" },
                new Models.Raitings { Id = 35, Students_Id = 11, Raiting = 6, TaskName = "Task3" },
                new Models.Raitings { Id = 36, Students_Id = 12, Raiting = 9, TaskName = "Task3" }
            };
            raitinglist.ForEach(s => studentContext.Raitings.Add(s));
            studentContext.SaveChanges();

            base.Seed(studentContext);
        }
    }
}
