﻿namespace Task3_Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Classes_Id = c.Int(nullable: false),
                        Classes_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.Classes_Id1)
                .Index(t => t.Classes_Id1);
            
            CreateTable(
                "dbo.Raitings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Students_Id = c.Int(nullable: false),
                        TaskName = c.String(),
                        Raiting = c.Single(nullable: false),
                        Students_Id1 = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Students", t => t.Students_Id1)
                .Index(t => t.Students_Id1);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Raitings", "Students_Id1", "dbo.Students");
            DropForeignKey("dbo.Students", "Classes_Id1", "dbo.Classes");
            DropIndex("dbo.Raitings", new[] { "Students_Id1" });
            DropIndex("dbo.Students", new[] { "Classes_Id1" });
            DropTable("dbo.Raitings");
            DropTable("dbo.Students");
            DropTable("dbo.Classes");
        }
    }
}
