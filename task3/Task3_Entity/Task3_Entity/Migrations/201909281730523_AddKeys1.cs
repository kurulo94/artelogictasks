﻿namespace Task3_Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKeys1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Students", "Classes_Id", c => c.Int(nullable: true));
            AlterColumn("dbo.Raitings", "Students_Id", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
        }
    }
}
