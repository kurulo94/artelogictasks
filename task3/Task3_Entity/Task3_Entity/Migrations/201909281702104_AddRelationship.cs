﻿namespace Task3_Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationship : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Students", "Classes_Id1", "dbo.Classes");
            DropForeignKey("dbo.Raitings", "Students_Id1", "dbo.Students");
            DropIndex("dbo.Students", new[] { "Classes_Id1" });
            DropIndex("dbo.Raitings", new[] { "Students_Id1" });
            DropColumn("dbo.Students", "Classes_Id");
            DropColumn("dbo.Raitings", "Students_Id");
            RenameColumn(table: "dbo.Students", name: "Classes_Id1", newName: "Classes_Id");
            RenameColumn(table: "dbo.Raitings", name: "Students_Id1", newName: "Students_Id");
            AlterColumn("dbo.Students", "Classes_Id", c => c.Int(nullable: true));
            AlterColumn("dbo.Raitings", "Students_Id", c => c.Int(nullable: true));
            CreateIndex("dbo.Students", "Classes_Id");
            CreateIndex("dbo.Raitings", "Students_Id");
            AddForeignKey("dbo.Students", "Classes_Id", "dbo.Classes", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Raitings", "Students_Id", "dbo.Students", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Raitings", "Students_Id", "dbo.Students");
            DropForeignKey("dbo.Students", "Classes_Id", "dbo.Classes");
            DropIndex("dbo.Raitings", new[] { "Students_Id" });
            DropIndex("dbo.Students", new[] { "Classes_Id" });
            AlterColumn("dbo.Raitings", "Students_Id", c => c.Int());
            AlterColumn("dbo.Students", "Classes_Id", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Raitings", name: "Students_Id", newName: "Students_Id1");
            RenameColumn(table: "dbo.Students", name: "Classes_Id", newName: "Classes_Id1");
            AddColumn("dbo.Raitings", "Students_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Students", "Classes_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Raitings", "Students_Id1");
            CreateIndex("dbo.Students", "Classes_Id1");
            AddForeignKey("dbo.Raitings", "Students_Id1", "dbo.Students", "Id");
            AddForeignKey("dbo.Students", "Classes_Id1", "dbo.Classes", "Id");
        }
    }
}
