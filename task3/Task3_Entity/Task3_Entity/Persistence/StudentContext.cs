namespace Task3_Entity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;
    using Task3_Entity.Models;

    public class StudentContext : DbContext
    {
        public StudentContext()
            : base("name=StudentContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        public DbSet<Classes> Classes { get; set; }
        public DbSet<Raitings> Raitings { get; set; }
        public DbSet<Students> Students { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Classes>().HasKey(x => x.Id);
            modelBuilder.Entity<Students>().HasKey(x =>x.Id);
            modelBuilder.Entity<Raitings>().HasKey(x =>x.Id);

            modelBuilder.Entity<Students>()
                .HasRequired(x=>x.Classes)
                .WithMany(x=>x.Students)
                .HasForeignKey(x=>x.Classes_Id);

            modelBuilder.Entity<Raitings>()
                .HasRequired(x =>x.Students)
                .WithMany(x=>x.Raitings)
                .HasForeignKey(x=>x.Students_Id);

            base.OnModelCreating(modelBuilder);
        }
    } 
}