﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity.Core.Repositories;
using Task3_Entity.Models;

namespace Task3_Entity.Persistence.Repositories
{
    public class StudentRepository : Repository<Students>, IStudentRepository
    {
        public StudentContext studentContext { get { return dbcontext as StudentContext; } }

        public StudentRepository(StudentContext studentContext) : base(studentContext) { }

        public List<string> GetThreeWorstStudents()
        {
            List<string> keyValuePairs = new List<string>();
            keyValuePairs = studentContext.Raitings.OrderBy(x => x.Raiting).Select(x => x.Students.FirstName + " " + x.Students.LastName).Take(3).ToList();
            return keyValuePairs;
        }
    }
}
