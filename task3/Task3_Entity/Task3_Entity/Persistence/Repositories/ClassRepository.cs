﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task3_Entity.Core.Repositories;
using Task3_Entity.Models;

namespace Task3_Entity.Persistence.Repositories
{
    public class ClassRepository : Repository<Classes>, IClassRepository
    {
        public StudentContext studentContext { get { return dbcontext as StudentContext; } }

        public ClassRepository(StudentContext Context) : base(Context) { }

        public List<string> GetTopThreeRaitEachClass()
        {
            List<string> keyValuePairs = new List<string>();
            int i = 1;
            for (; i <= studentContext.Classes.Count(); i++)
            {
                string tmp = studentContext.Classes.Where(x => x.Id == i).Select(x => x.Name).Single() + " class with top raiting:";
                var k = (studentContext.Raitings.Where(x => x.Students.Classes_Id == i).OrderByDescending(x => x.Raiting).Select(x => x.Raiting).Take(3).ToList());
                foreach (var item in k) { tmp += item.ToString() + " "; }
                keyValuePairs.Add(tmp);
            }
            return keyValuePairs;
        }
    }
}
