﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity.Core.Repositories;
using Task3_Entity.Models;

namespace Task3_Entity.Persistence.Repositories
{
    public class RaitingRepository : Repository<Raitings>, IRaitingRepository
    {
        public StudentContext studentContext { get { return dbcontext as StudentContext; } }

        public RaitingRepository(StudentContext Context) : base(Context) { }

        public float GetAvrRaitAll()
        {
            var avr = studentContext.Raitings.Average(s => s.Raiting);
            return avr;
        }

        public float GetAvrRaitForClasses(string ClassName)
        {
            if (ClassName != null)
            {
                var avgc = studentContext.Raitings.Where(X => X.Students.Classes.Name == ClassName).Average(s => s.Raiting);
                return avgc;
            }
            else throw new ArgumentNullException();
        }

        public float GetAvrRaitForTask(string TaskName)
        {
            if (TaskName != null)
            {
                var avrt = studentContext.Raitings.Where(x => x.TaskName == TaskName).Average(s => s.Raiting);
                return avrt;
            }
            else throw new ArgumentNullException();
        }
    }
}
