﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity.Models;

namespace Task3_Entity.Persistence.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext dbcontext;

        public Repository(DbContext context)
        {
            dbcontext = context;
        }

        public void Add(TEntity entity)
        {
            dbcontext.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            dbcontext.Set<TEntity>().AddRange(entities);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return dbcontext.Set<TEntity>().Where(predicate);
        }

        public TEntity Get(int id)
        {
            return dbcontext.Set<TEntity>().Find(1);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return dbcontext.Set<TEntity>().ToList();
        }

        public void Remove(TEntity entity)
        {
            dbcontext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            dbcontext.Set<TEntity>().RemoveRange(entities);
        }
    }
}
