﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity.Core;
using Task3_Entity.Core.Repositories;
using Task3_Entity.Persistence.Repositories;

namespace Task3_Entity.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly StudentContext _context;

        public UnitOfWork(StudentContext studentContext)
        {
            _context = studentContext;
            Classes = new ClassRepository(_context);
            Students = new StudentRepository(_context);
            Raitings = new RaitingRepository(_context);
        }

        public IClassRepository Classes { get; private set; }
        public IStudentRepository Students { get; private set; }
        public IRaitingRepository Raitings { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
