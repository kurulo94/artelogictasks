﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentsRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity;
using Task3_Entity.Persistence;

namespace StudentsRating.Services.Tests
{
    [TestClass()]
    public class StudentServiseTests
    {
        #region setup
        float avrall;
        float avrClass;
        float avrTask;
        List<string> topthreeeachclass;
        List<string> threeworststudents;

        float avrall1;
        float avrClass1;
        float avrTask1;
        List<string> topthreeeachclass1;
        List<string> threeworststudents1;
        StudentContext studentContext = new StudentContext();
        StudentServise studentServise;

        List<string> threeworststudentsTest = new List<string> { "Petro Kaban", "Valerian Gnatiyk", "Vitas Mask" };
        List<string> topthreeeachclassTest = new List<string> { "1b class with top raiting:12 12 11 ", "2b class with top raiting:12 11 11 " };
        float[] AvrTasknametest = new float[] { 4, 10, 1, 2, 6, 7, 9, 10, 12, 1, 3, 5 };
        float[] AvrClassestest = new float[] { 4, 10, 1, 2, 6, 7, 2, 11, 11, 12, 3, 4, 9, 11, 12, 3, 5, 8 };
        float[] Avralltest = new float[] { 4, 10, 1, 2, 6, 7, 9, 10, 12, 1, 3, 5, 2, 11, 11, 12, 3, 4, 5, 11, 10, 11, 5, 6, 9, 11, 12, 3, 5, 8, 1, 1, 11, 6, 6, 9 };
        #endregion
        [TestMethod()]
        public void StudentServise1()
        {
            studentServise = new StudentServise(studentContext);
            Assert.IsNotNull(studentServise);
        }

        [TestMethod()]
        public void GetAvrRaitAllTest1()
        {
            studentServise = new StudentServise(studentContext);
            avrall1 = studentServise.GetAvrRaitAll();
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                avrall = unitOfWork.Raitings.GetAvrRaitAll();
            }
            Assert.AreEqual(avrall, avrall1);
            Assert.AreEqual(avrall, Avralltest.Average());
            Assert.AreEqual(avrall1, Avralltest.Average());
        }

        [TestMethod()]
        public void GetAvrRaitForClassesTest1()
        {
            studentServise = new StudentServise(studentContext);
            avrClass1 = studentServise.GetAvrRaitForClasses("1b");
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                avrClass = unitOfWork.Raitings.GetAvrRaitForClasses("1b");
            }
            Assert.AreEqual(avrClass, avrClass1);
            Assert.AreEqual(AvrClassestest.Average(),avrClass);
            Assert.AreEqual(AvrClassestest.Average(), avrClass1);
        }

        [TestMethod()]
        public void GetAvrRaitForTaskTest1()
        {
            studentServise = new StudentServise(studentContext);
            avrTask1 = studentServise.GetAvrRaitForTask("Task1");
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                avrTask = unitOfWork.Raitings.GetAvrRaitForTask("Task1");
            }
            Assert.AreEqual(avrTask, avrTask1);
            Assert.AreEqual(avrTask, AvrTasknametest.Average());
            Assert.AreEqual(avrTask1, AvrTasknametest.Average());
        }

        [TestMethod()]
        public void GetTopThreeRaitEachClassTest1()
        {
            studentServise = new StudentServise(studentContext);
            topthreeeachclass1 = studentServise.GetTopThreeRaitEachClass();
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                topthreeeachclass = unitOfWork.Classes.GetTopThreeRaitEachClass();
            }
            CollectionAssert.AreEqual(topthreeeachclass, topthreeeachclass1);
            CollectionAssert.AreEqual(topthreeeachclass, topthreeeachclassTest);
            CollectionAssert.AreEqual(topthreeeachclass1, topthreeeachclassTest);
        }

        [TestMethod()]
        public void GetThreeWorstStudentsTest1()
        {
            studentServise = new StudentServise(studentContext);
            threeworststudents1 = studentServise.GetThreeWorstStudents();
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                threeworststudents = unitOfWork.Students.GetThreeWorstStudents();
            }
            CollectionAssert.AreEqual(threeworststudents, threeworststudents1);
            CollectionAssert.AreEqual(threeworststudents, threeworststudentsTest);
            CollectionAssert.AreEqual(threeworststudents1, threeworststudentsTest);
        }
    }
}

