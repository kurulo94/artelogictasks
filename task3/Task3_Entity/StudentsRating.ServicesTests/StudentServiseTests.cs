﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentsRating.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task3_Entity;
using Task3_Entity.Models;
using Task3_Entity.Persistence;

namespace StudentsRating.Services.Tests
{
    [TestClass()]
    public class StudentServiseTests
    {
        #region setup
        float avrall;
        float avrClass;
        float avrTask;
        List<float[]> topthreeeachclass;
        List<string> threeworststudents;

        float avrall1;
        float avrClass1;
        float avrTask1;
        List<float[]> topthreeeachclass1;
        List<string> threeworststudents1;
        StudentContext studentContext = new StudentContext();
        StudentServise studentServise;
        #endregion        
        [TestMethod()]
        public void StudentServise1()
        {
            studentServise = new StudentServise(studentContext);
            Assert.IsNotNull(studentServise);
        }

        [TestMethod()]
        public void GetAvrRaitAllTest1()
        {
            studentServise = new StudentServise(studentContext);
            avrall1 = studentServise.GetAvrRaitAll();
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                avrall = unitOfWork.Raitings.GetAvrRaitAll();
            }
            Assert.AreEqual(avrall, avrall1);
        }

        [TestMethod()]
        public void GetAvrRaitForClassesTest1()
        {
            studentServise = new StudentServise(studentContext);
            avrClass1 = studentServise.GetAvrRaitForClasses("1b");
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                avrClass = unitOfWork.Raitings.GetAvrRaitForClasses("1b");
            }
            Assert.AreEqual(avrClass, avrClass1);
        }

        [TestMethod()]
        public void GetAvrRaitForTaskTest1()
        {
            studentServise = new StudentServise(studentContext);
            avrTask1 = studentServise.GetAvrRaitForTask("Task1");
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                avrTask = unitOfWork.Raitings.GetAvrRaitForTask("Task1");
            }
            Assert.AreEqual(avrTask, avrTask1);
        }

        [TestMethod()]
        public void GetTopThreeRaitEachClassTest1()
        {
            studentServise = new StudentServise(studentContext);
            topthreeeachclass1 = studentServise.GetTopThreeRaitEachClass();
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                topthreeeachclass = unitOfWork.Classes.GetTopThreeRaitEachClass();
            }
            CollectionAssert.AreEqual(topthreeeachclass, topthreeeachclass1);
        }

        [TestMethod()]
        public void GetThreeWorstStudentsTest1()
        {
            studentServise = new StudentServise(studentContext);
            threeworststudents1 = studentServise.GetThreeWorstStudents();
            using (UnitOfWork unitOfWork = new UnitOfWork(studentContext))
            {
                threeworststudents = unitOfWork.Students.GetThreeWorstStudents();
            }
            CollectionAssert.AreEqual(threeworststudents, threeworststudents1);
        }
    }
}