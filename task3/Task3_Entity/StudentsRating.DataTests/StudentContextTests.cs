﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task3_Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3_Entity.Tests
{
    [TestClass()]
    public class StudentContextTests
    {
        [TestMethod()]
        public void SetDefaultDataTest()
        {
            StudentContext.SetDefaultData();
            Assert.AreEqual("","");
        }
    }
}