﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Google_maps_route.Interfaces;
using Google_maps_route.Models.Bing;
using Newtonsoft.Json;

namespace Google_maps_route
{
    public class BingMapRoutes : IMapsRoutesProvider,IMaps,ITest
    {
        public string authenticationResultCode { get; set; }
        public string brandLogoUri { get; set; }
        public string copyright { get; set; }
        public List<ResourceSet> resourceSets { get; set; }
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
        public string traceId { get; set; }
         
        public bool IsConnected { get { if (this.statusCode == 1) return true; else return false; } }
        public bool IsWriteForTest { get; set; }
        private UserCredentials Credentials { get; set; }

        public BingMapRoutes()
        {

        }

        public BingMapRoutes(UserCredentials userCredentials)
        {
            Credentials = new UserCredentials(userCredentials.Name,userCredentials.Token);
        }           

        public  IMaps GetRoutes(string original, string destination)
        {
            string URL = "http://dev.virtualearth.net/REST/V1/Routes/Driving?o=json&wp.0=" +
            original + "&wp.1=" + destination +
            "&optmz=distance&rpo=Points&key=" + Credentials.Token;//?
            WebRequest request = WebRequest.Create(URL);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string responseFromServer;
            using (StreamReader reader = new StreamReader(data))
            {
                 responseFromServer = reader.ReadToEnd();
            }
            response.Close();
            if (this.IsWriteForTest) { WriteTMPfile(responseFromServer); }
            var mapRoutes = JsonConvert.DeserializeObject<BingMapRoutes>(responseFromServer);
            this.authenticationResultCode = mapRoutes.authenticationResultCode;
            this.brandLogoUri = mapRoutes.brandLogoUri;
            this.copyright = mapRoutes.copyright;
            this.resourceSets = mapRoutes.resourceSets;
            this.statusCode = mapRoutes.statusCode;
            this.statusDescription = mapRoutes.statusDescription;
            this.traceId = mapRoutes.traceId;
            return this;
        }

        public void RegisterUser(UserCredentials userCredentials)
        {
            Credentials = new UserCredentials(userCredentials.Name, userCredentials.Token);
        }

        private void WriteTMPfile(string response_data)
        {
            if (File.Exists(@"bing.txt")) { File.Delete(@"bing.txt"); }
            File.WriteAllText(@"bing.txt", response_data);
        }

        public void SetTest()
        { this.IsWriteForTest = true; }
    }
}
