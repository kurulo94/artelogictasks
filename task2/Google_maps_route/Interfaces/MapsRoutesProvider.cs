﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Google_maps_route.Interfaces
{
    public interface IMapsRoutesProvider
    {
        IMaps GetRoutes(string original, string destination);
        void RegisterUser(UserCredentials userCredentials);
        void SetTest();
    }
     
}
