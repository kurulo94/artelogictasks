﻿using Google_maps_route.Interfaces;
using System.Collections;
using System.Collections.Generic;

namespace Google_maps_route
{
    public class MapRoutes:IEnumerable<IMapsRoutesProvider>
    {
        private readonly IList<IMapsRoutesProvider> _mapsRoutes;

        public void SetTest()
        {
            foreach (var map in _mapsRoutes)
            {
                map.SetTest();
            }
        }

        public MapRoutes()
        {
            _mapsRoutes =new List<IMapsRoutesProvider>();
        }     

        public void GetRoutes(string original,string destination)
        {
            foreach (var map in _mapsRoutes)
            {
                map.GetRoutes(original, destination);
            }            
        }

        public void RegisterMapRoutes(IMapsRoutesProvider provider)
        {
            _mapsRoutes.Add(provider);
        }

        public IEnumerator<IMapsRoutesProvider> GetEnumerator()
        {
            foreach (var item in _mapsRoutes) { yield return item; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _mapsRoutes.GetEnumerator();
        }     

    }
}
