﻿namespace Google_maps_route
{
    public class StartLocation
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
