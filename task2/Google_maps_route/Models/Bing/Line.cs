﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class Line
    {
        public string type { get; set; }
        public List<List<double>> coordinates { get; set; }
    }
}
