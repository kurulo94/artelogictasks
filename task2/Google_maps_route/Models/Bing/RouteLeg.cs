﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class RouteLeg
    {
        public ActualEnd actualEnd { get; set; }
        public ActualStart actualStart { get; set; }
        public List<object> alternateVias { get; set; }
        public int cost { get; set; }
        public string description { get; set; }
        public EndLocation endLocation { get; set; }
        public List<ItineraryItem> itineraryItems { get; set; }
        public string routeRegion { get; set; }
        public List<RouteSubLeg> routeSubLegs { get; set; }
        public StartLocation startLocation { get; set; }
        public double travelDistance { get; set; }
        public int travelDuration { get; set; }
    }
}
