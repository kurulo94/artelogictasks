﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class ItineraryItem
    {
        public string compassDirection { get; set; }
        public List<Detail> details { get; set; }
        public string exit { get; set; }
        public string iconType { get; set; }
        public Instruction instruction { get; set; }
        public bool isRealTimeTransit { get; set; }
        public ManeuverPoint maneuverPoint { get; set; }
        public int realTimeTransitDelay { get; set; }
        public string sideOfStreet { get; set; }
        public string tollZone { get; set; }
        public string towardsRoadName { get; set; }
        public string transitTerminus { get; set; }
        public double travelDistance { get; set; }
        public int travelDuration { get; set; }
        public string travelMode { get; set; }
        public List<Warning> warnings { get; set; }
        public List<string> signs { get; set; }
        public List<Hint> hints { get; set; }
    }
}
