﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class StartLocation
    {
        public List<double> bbox { get; set; }
        public string name { get; set; }
        public Point2 point { get; set; }
        public Address2 address { get; set; }
        public string confidence { get; set; }
        public string entityType { get; set; }
        public List<GeocodePoint2> geocodePoints { get; set; }
        public List<string> matchCodes { get; set; }
    }
}
