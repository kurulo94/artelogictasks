﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class Detail
    {
        public int compassDegrees { get; set; }
        public List<int> endPathIndices { get; set; }
        public List<string> locationCodes { get; set; }
        public string maneuverType { get; set; }
        public string mode { get; set; }
        public List<string> names { get; set; }
        public string roadType { get; set; }
        public List<int> startPathIndices { get; set; }
        public RoadShieldRequestParameters roadShieldRequestParameters { get; set; }
    }
}
