﻿namespace Google_maps_route.Models.Bing
{
    public class Instruction
    {
        public object formattedText { get; set; }
        public string maneuverType { get; set; }
        public string text { get; set; }
    }
}
