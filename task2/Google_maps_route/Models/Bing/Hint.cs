﻿namespace Google_maps_route.Models.Bing
{
    public class Hint
    {
        public string hintType { get; set; }
        public string text { get; set; }
    }
}
