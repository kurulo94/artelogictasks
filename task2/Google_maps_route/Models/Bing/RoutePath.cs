﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class RoutePath
    {
        public List<object> generalizations { get; set; }
        public Line line { get; set; }
    }
}
