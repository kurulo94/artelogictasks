﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class Shield
    {
        public List<string> labels { get; set; }
        public int roadShieldType { get; set; }
    }
}
