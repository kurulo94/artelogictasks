﻿namespace Google_maps_route.Models.Bing
{
    public class Address
    {
        public string adminDistrict { get; set; }
        public string countryRegion { get; set; }
        public string formattedAddress { get; set; }
        public string locality { get; set; }
    }
}
