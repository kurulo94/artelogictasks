﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class GeocodePoint
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
        public string calculationMethod { get; set; }
        public List<string> usageTypes { get; set; }
    }
}
