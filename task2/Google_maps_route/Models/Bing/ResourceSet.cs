﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class ResourceSet
    {
        public int estimatedTotal { get; set; }
        public List<Resource> resources { get; set; }
    }
}
