﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class RoadShieldRequestParameters
    {
        public int bucket { get; set; }
        public List<Shield> shields { get; set; }
    }
}
