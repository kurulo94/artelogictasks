﻿using System.Collections.Generic;

namespace Google_maps_route.Models.Bing
{
    public class ManeuverPoint
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }
}
