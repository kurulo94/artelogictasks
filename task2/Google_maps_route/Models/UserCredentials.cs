﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Google_maps_route
{
    public class UserCredentials
    {
      public  string Name { get;   }
      public  string Token { get;  }
      public  DateTime SesionStart { get; }
      public  DateTime SessionEnd { get; set; }
        
        public UserCredentials()
        {
            SesionStart = DateTime.Now;
        }
        //!? Why i cant use  param in deconstructor without set
        ~UserCredentials()
        {
            SessionEnd = DateTime.Now;
        }

        public UserCredentials(string name, string token)
        {
            Name = name;
            Token = token;
            SesionStart = DateTime.Now;
        }
    }
}
