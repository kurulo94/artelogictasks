﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Google_maps_route.Interfaces;
using System.Data.SqlClient;

namespace Google_maps_route
{

    public class GoogleMapRoutes:IMapsRoutesProvider,IMaps,ITest
    {
        public List<GeocodedWaypoint> geocoded_waypoints { get; set; }
        public List<Route> routes { get; set; }
        public string status { get; set; }

        public bool IsConnected { get { if (this.status == "OK") return true; else return false; } }
        public bool IsWriteForTest { get ; set; }
        private UserCredentials Credentials;

        public GoogleMapRoutes()
        {

        }

        public GoogleMapRoutes(UserCredentials userCredentials)
        {
            Credentials = new UserCredentials(userCredentials.Name,userCredentials.Token);
        }

        public IMaps GetRoutes(string original, string destination)
        {
            string url = @"https://maps.googleapis.com/maps/api/directions/json?origin=" + original + "&destination=" + destination + "&key="+Credentials.Token;//?";
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string responseFromServer;
            using (StreamReader reader = new StreamReader(data))
            {
                responseFromServer = reader.ReadToEnd();
            }
            if (IsWriteForTest) { WriteTMPfile(responseFromServer); }
            response.Close();
            var mapRoutes = JsonConvert.DeserializeObject<GoogleMapRoutes>(responseFromServer);
            this.geocoded_waypoints = mapRoutes.geocoded_waypoints;
            this.routes = mapRoutes.routes;
            this.status = mapRoutes.status;
            return this;
        }

        public void RegisterUser(UserCredentials userCredentials)
        {
            Credentials = new UserCredentials(userCredentials.Name, userCredentials.Token);
        }

        private void WriteTMPfile(string response_data)
        {
            if (File.Exists(@"google.txt")) { File.Delete(@"google.txt"); }
            File.WriteAllText(@"google.txt", response_data);
        }

        public void SetTest()
        { this.IsWriteForTest = true; }
    }
}
