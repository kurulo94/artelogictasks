﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Google_maps_route;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Google_maps_route.Tests
{
    
    [TestClass()]
    public class GoogleMapRoutesTests
    {
        public const string GoogleToken = "AIzaSyCdZWrnHonxc8YjAhIYExB0P8RhDf3DX2c";
        public const string BingToken = "AvleZOvF9Zaei3tTu415peficHHK71s0IOpQHLbQfYY2qk8DWtEKYTF_v4C9XWZj";


        [TestMethod()]
        public void GetMapRoutesTestAreEqualSameCreatedClasses()
        {
            #region test first group
            UserCredentials userCredentialsGoogle = new UserCredentials("Roman",GoogleToken);
            UserCredentials userCredentialsBing = new UserCredentials("Roman", BingToken);
            GoogleMapRoutes google = new GoogleMapRoutes();
            BingMapRoutes bing = new BingMapRoutes();
            bing.RegisterUser(userCredentialsBing);
            google.RegisterUser(userCredentialsGoogle);                       
            bing.GetRoutes("Lviv", "Rivne");
            google.GetRoutes("Lviv", "Rivne");
            #endregion
            #region test second group
            var routes = new MapRoutes();
            routes.RegisterMapRoutes(new GoogleMapRoutes(new UserCredentials("Ivan", GoogleToken)));            
            routes.RegisterMapRoutes(new BingMapRoutes(new UserCredentials("Vasia", BingToken))); 
            routes.GetRoutes("Lviv","Rivne");
            GoogleMapRoutes googleMap=new GoogleMapRoutes();
            BingMapRoutes bingMap = new BingMapRoutes();
            foreach (var item in routes)
            {
                if (item is GoogleMapRoutes) { googleMap = item as GoogleMapRoutes; }
                if (item is BingMapRoutes) { bingMap = item as BingMapRoutes; }
            }
            #endregion
            Assert.AreEqual(bing.brandLogoUri,bingMap.brandLogoUri);
            Assert.AreEqual(google.geocoded_waypoints[0].place_id,googleMap.geocoded_waypoints[0].place_id);   
        }

        [TestMethod()]
        public void GetMapRoutesAreTrueDeserialization()
        {
            var routes = new MapRoutes();
            routes.RegisterMapRoutes(new GoogleMapRoutes(new UserCredentials("Ivan", GoogleToken)));
            routes.RegisterMapRoutes(new BingMapRoutes(new UserCredentials("Vasia",BingToken)));
            routes.SetTest();//set prop for all classes as true(write)
            routes.GetRoutes("Lviv", "Rivne");
            GoogleMapRoutes googleMap = new GoogleMapRoutes();
            BingMapRoutes bingMap = new BingMapRoutes();
            foreach (var item in routes)
            {
                if (item is GoogleMapRoutes) { googleMap = item as GoogleMapRoutes; }
                if (item is BingMapRoutes) { bingMap = item as BingMapRoutes; }
            }
            //
            string GoogleStr;
            string BingStr;
            using (StreamReader sr = new StreamReader(@"google.txt"))
            {
                GoogleStr = sr.ReadToEnd();
            }
            string Gstr = GoogleStr.Substring(GoogleStr.IndexOf("\"overview_polyline\""), GoogleStr.IndexOf("\"summary\"")- GoogleStr.IndexOf("\"overview_polyline\"")).Replace("\"overview_polyline\" : {\n            \"points\" : \"", "").Replace("\"\n         },\n         ", "").Replace("\\\\","\\");
            using (StreamReader sr = new StreamReader(@"bing.txt"))
            {
                BingStr = sr.ReadToEnd();
            }
            string Bstr=BingStr.Substring(BingStr.IndexOf("\"traceId\":\"")+11, BingStr.Length- BingStr.IndexOf("\"traceId\":\"")-13);

            Assert.AreEqual(googleMap.routes[0].overview_polyline.points,Gstr);
            Assert.AreEqual(bingMap.traceId,Bstr);
        }
    }
}