﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Create_a_Structure
{
    public class Structure_Pattern
    {
        public static List<string> SplitBrackets(string str)
        {
            List<string> ret = new List<string>();
            int start = 0;
            int derive = 0;
            int i = 0;
            for (; i < str.Length; i++)
            {
                if (str[i] == '[') { derive++; }
                if (str[i] == ']')
                {
                    derive--;
                    if (derive == 0)
                    {
                        ret.Add(str.Substring(start, i - start + 1));                        
                        i++; i++;
                        start = i;
                    }
                }
            }
            if (i - 2 != str.Length)
            { ret.Add(str.Substring(start)); }
            return ret;
        }
        public static void Do_Structure_pattern(string path, string directory_pattern)
        {
            Directory.CreateDirectory(path);
            var split_list = SplitBrackets(directory_pattern);
            if (split_list.Count > 0)
            {
                foreach (var item in split_list)
                {
                    if (item.Contains("[") & item.Contains("]") & item!=null)
                    {
                        string starts = item.Substring(0,item.IndexOf('[')).Trim();
                        var newpath = path + Path.DirectorySeparatorChar + starts;
                        Do_Structure_pattern(newpath, item.Substring(starts.Length + 1, item.Length - starts.Length - 2));
                    }
                    else  { Create_List_Directory(item, path); }
                }
            }
            else  { Create_List_Directory(directory_pattern,path); }
        }
        public static void Create_List_Directory(string pattern,string path)
        {
            var list = pattern.Split(',');
            foreach (var item in list)
            {
                Directory.CreateDirectory(path + Path.DirectorySeparatorChar + item);
            }
        }
    }
}
