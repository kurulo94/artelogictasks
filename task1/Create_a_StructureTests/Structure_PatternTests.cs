﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Create_a_Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

namespace Create_a_Structure.Tests
{
    [TestClass()]
    public class StructurePatternTests
    {
        [TestMethod()]
        public void SplitBracketsTestAreEqual()
        {
            string directory_pattern = "1[11[111,112],12], 2[21[22[23[26]]]], 3[hello word, 32, 33]";
            var actual = Structure_Pattern.SplitBrackets(directory_pattern);
            List<string> expected = new List<string>() { "1[11[111,112],12]", " 2[21[22[23[26]]]]", " 3[hello word, 32, 33]" };
            CollectionAssert.AreEqual(expected, actual, StructuralComparisons.StructuralComparer);
        }
        [TestMethod()]
        public void Do_Structure_patternTestAreSame()
        {
            string path = "test2";
            string directory_pattern = "1[11[111,112],12]";
            Structure_Pattern.Do_Structure_pattern(path, directory_pattern);
            Assert.IsTrue(Directory.Exists(path));
            Assert.IsTrue(Directory.Exists(path + Path.DirectorySeparatorChar + "1\\11"));
            Assert.IsTrue(Directory.Exists(path + Path.DirectorySeparatorChar + "1\\11\\111"));
            Assert.IsTrue(Directory.Exists(path + Path.DirectorySeparatorChar + "1\\11\\112"));
            Assert.IsTrue(Directory.Exists(path + Path.DirectorySeparatorChar + "1\\12"));
        }

        [TestMethod()]
        public void Create_List_DirectoryTest()
        {
            string path = "test3";
            string pattern = "111,112";
            Structure_Pattern.Create_List_Directory(pattern, path);
            Assert.IsTrue(Directory.Exists(path + Path.DirectorySeparatorChar + "111"));
        }
    }
}