mapboxgl.accessToken = 'pk.eyJ1Ijoia3VydWxvOTQiLCJhIjoiY2sxdjJvNmlrMTN5NDNidDYzNmd1bXB3dyJ9.cmQqJSkqspHREpVn8aFgbQ';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [24.03121737685092,49.84202824089698],
    zoom: 10,
    interactive:true
});
map.doubleClickZoom.disable();    
map.addControl(new MapboxGeocoder({
    accessToken: mapboxgl.accessToken,
    mapboxgl: mapboxgl 
}));

//get gorm from html
let popup_form=document.getElementById("popup-form");
popup_form.style.display='block';

// create the popup
var popup = new mapboxgl.Popup({ offset: 35 })
.setDOMContent(popup_form);

//create marker and set popup
var marker = new mapboxgl.Marker({
draggable: true
})
.setLngLat([24.03121737685092,49.84202824089698])//Lviv
.addTo(map); 

var pointsArray = new Array();

//init list items when start
firstInit();

function firstInit(){
    if(localStorage.getItem('points')!=null){
        const tmpArray=localStorage.getItem('points');
        pointsArray=JSON.parse(tmpArray);
         for(let i=0;i<pointsArray.length;i++){           
            initPlacesList(pointsArray[i][0],pointsArray[i][1],pointsArray[i][2]);             
            } 
}};

function initPlacesList(addedPlace,lng,lat){       
    document.getElementById("ul-container")
    .innerHTML+='<div id=\"'+addedPlace+'\"><li class=\"list-item\" onclick=\"viewPlace('+lng+','+lat+','+addedPlace+')\">'+addedPlace+'</li></div>'
    +'<button class="btn" id=\"delete-button-'+addedPlace+'" onclick=\"remove_selected_item('+addedPlace+')\" type=\"button\"><i class=\"icon ion-md-close\"></i></button>';        
}

function initMarker (e) {     
        popup_form.lastElementChild.setAttribute( "onClick", "addPlace();" );
        popup_form.firstElementChild.innerHTML="Do you wanna create your place?"; 
        marker.setLngLat([e.lngLat.lng,e.lngLat.lat]);  
        popup.setLngLat([e.lngLat.lng,e.lngLat.lat])
             .setHTML(popup_form.innerHTML)
             .addTo(map);
}; 

function addPlace(){
    let lngLat = marker.getLngLat();
    let addedPlace= document.getElementById("input-place").value; 
    if(addedPlace!=''){
        if(chech_for_added(addedPlace)!=true){
            // 
            let PlacePoint=new Array();
            PlacePoint.push(addedPlace,lngLat.lng,lngLat.lat);
            pointsArray.push(PlacePoint);
            localStorage.setItem('points',JSON.stringify(pointsArray) );
            //
        document.getElementById("input-place").value='';
        initPlacesList(addedPlace,lngLat.lng,lngLat.lat);  
        return false;
        }
    }
    else return alert("Empty value!")
} 

function chech_for_added(addedPlace){
    let flag=false;
    for(let i=0;i<pointsArray.length; i++) {
         if(pointsArray[i][0]==addedPlace) {
                alert("Same object!");
                flag=true;
                break;}
        } 
     return flag;   
}

let startInput='';//value of input before edit, set when viewPlace
function editPlace(){   
    let addedPlace= document.getElementById("input-place").value; 
    if(chech_for_added(addedPlace)==false){ 
            remove_from_array_by_name(startInput);
            document.getElementById(startInput).remove();
            document.getElementById('delete-button-'+startInput+'').remove();  
            addPlace(); 
            }  
    };
     
function viewPlace(lng,lat, addedplace){
        marker.setLngLat([lng,lat]);    
        popup_form.lastElementChild.setAttribute( "onClick", "editPlace();" );
        popup_form.firstElementChild.innerHTML="Do you wanna edit your place?";    
        popup.closeOnClick=false;
        popup.setLngLat([lng,lat])
          .setHTML(popup_form.innerHTML)
          .addTo(map);    
        document.getElementById("input-place").value=addedplace.id;   
        startInput=addedplace.id;    
        map.flyTo({
        center: [lng,lat],
        zoom: 13
        });        
        return false;
}; 

function remove_selected_item(li){
        remove_from_array_by_name(li.id);
        document.getElementById(li.id).remove();
        document.getElementById('delete-button-'+li.id+'').remove();    
}

function remove_from_array_by_name(name){
      for(let i=0;i<pointsArray.length; i++) {
                if(pointsArray[i][0]==name){
                    pointsArray.splice(i,1);
                   break;
                   }
            }
    localStorage.setItem('points',JSON.stringify(pointsArray));   
}

map.on('click', initMarker);   
    
map.addControl(new mapboxgl.NavigationControl());
map.addControl(new mapboxgl.GeolocateControl({
positionOptions: {
enableHighAccuracy: true
},
trackUserLocation: true
}));