﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Task6.Areas.IdentifyUser.Models;
using Task6.Models;
using Task6.Models.Interfaces;

namespace Task6.Services
{
    public class GMailService:IMailService,IDisposable
    {
        public static string User { get; set; }
        private GmailService Service { get; set; }
        public GMailService()
        {
            string[] Scopes = { GmailService.Scope.GmailReadonly };
            UserCredential credential;
            using (var stream = new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = "token.json";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                                Scopes,
                                User,
                                CancellationToken.None,
                                new FileDataStore(credPath, true)).Result;
            }
            var _service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Task6",
            });
            Service=_service;
        }
        public List<MessagesViewModel> ListMessages(String query)
        {
            List<MessagesViewModel> ResultSearchVM = new List<MessagesViewModel>();
            List<Message> messages = new List<Message>();
            UsersResource.MessagesResource.ListRequest request = Service.Users.Messages.List(User);
            request.Q = query;
            do
            {               
                ListMessagesResponse response = request.Execute();
                if (response.Messages == null) { break; }
                messages.AddRange(response.Messages);
                    request.PageToken = response.NextPageToken;   
            } while (!String.IsNullOrEmpty(request.PageToken));

            foreach (var email in messages)
            {
                var emailInfoRequest = Service.Users.Messages.Get(User, email.Id);
                var emailInfoResponse = emailInfoRequest.Execute();
                if (emailInfoResponse != null)
                {
                    String from = "";
                    String date = "";
                    String subject = "";
                    foreach (var mParts in emailInfoResponse.Payload.Headers)
                    {
                        if (mParts.Name == "Date")
                        {
                            //"Thu, 7 Nov 2019 10:50:12 +0000 (GMT)"
                            date = mParts.Value;
                        }
                        else if (mParts.Name == "From")
                        {
                            from = mParts.Value;
                        }
                        else if (mParts.Name == "Subject")
                        {
                            subject = mParts.Value;
                        }

                        if (date != "" && from != "" && subject!="")
                        {
                            string decodedString = "";
                            if (emailInfoResponse.Payload.Parts != null)
                            {
                                foreach (MessagePart p in emailInfoResponse.Payload.Parts)
                                {
                                    if (p.MimeType == "text/html")
                                    {
                                        byte[] data = FromBase64ForUrlString(p.Body.Data);
                                        decodedString = System.Text.Encoding.UTF8.GetString(data);
                                        ResultSearchVM.Add(new MessagesViewModel(subject, from, date, decodedString));
                                        from = "";
                                        date = "";
                                        subject = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return ResultSearchVM;
        }

        private byte[] FromBase64ForUrlString(string base64ForUrlInput)
        {
            int padChars = (base64ForUrlInput.Length % 4) == 0 ? 0 : (4 - (base64ForUrlInput.Length % 4));
            System.Text.StringBuilder result = new System.Text.StringBuilder(base64ForUrlInput, base64ForUrlInput.Length + padChars);
            result.Append(String.Empty.PadRight(padChars, '='));
            result.Replace('-', '+');
            result.Replace('_', '/');
            return Convert.FromBase64String(result.ToString());
        }

        public void Dispose()
        {
            this.Service.Dispose();
        }
    }
}
