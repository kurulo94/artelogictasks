﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task6.Areas.IdentifyUser.Models
{    
    /// <summary>
    /// Model class for valid input data
    /// </summary>
    public class SearchModel
    {
        [Required]
        public string SearchQuery { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }     
    }
}
