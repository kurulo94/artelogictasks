﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task6.Models
{
    /// <summary>
    /// Inheritance from base IdentityUser. Additional params for user/ login, register..
    /// </summary>
    public class ApplicationUser:IdentityUser
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }
}
