﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task6.Models.Interfaces;

namespace Task6.Models.ViewModels
{
    /// <summary>
    /// View model for correct binding data use for pagination
    /// </summary>
    public class MessagesListViewModel
    {
        public List<MessagesViewModel>  Messages { get; set; }
        public PagingInfo PagingInfo  { get; set; }
    }
}
