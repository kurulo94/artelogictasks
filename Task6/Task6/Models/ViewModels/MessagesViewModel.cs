﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task6.Models
{
    /// <summary>
    /// View model for correct view messages
    /// </summary>
    public class MessagesViewModel
    {
        public string Title { get; set; }
        public string From { get; set; }
        public string ReceivedDate { get; set; }
        public string Content { get; set; }
        public MessagesViewModel(string _Title, string _From, string _ReceivedDate, string _Content)
        {
            Title = _Title;
            From = _From;
            ReceivedDate = _ReceivedDate;
            Content = _Content;
        }
        public MessagesViewModel()
        {

        }
    }
}
