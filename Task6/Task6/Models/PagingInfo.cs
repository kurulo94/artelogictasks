﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task6.Models
{
    /// <summary>
    /// Model for pagination with logic params. Use in the Paging
    /// </summary>
    public class PagingInfo
    {
        public int TotalItem { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public string urlParam { get; set; }
        //If need
        //public int totalPage => (int)Math.Ceiling((decimal)TotalItem / ItemsPerPage);
    }
}
