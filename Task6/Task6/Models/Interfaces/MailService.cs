﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task6.Models.Interfaces
{
    public interface IMailService
    {
         List<MessagesViewModel> ListMessages(string query);
    }
}
