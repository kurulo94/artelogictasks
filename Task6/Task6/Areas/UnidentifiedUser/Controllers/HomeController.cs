﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Task6.Areas.UnidentifiedUser.Controllers
{
    [Area("UnidentifiedUser")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            bool isAuthenticated = User.Identity.IsAuthenticated;
            if (!isAuthenticated)
            {
                return View();
            }
            return Redirect("IdentifyUser/Home/Index");
        }
    }
}