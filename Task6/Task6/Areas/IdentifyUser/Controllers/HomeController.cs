﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Task6.Areas.IdentifyUser.Models;
using Task6.Data;
using Task6.Models;
using Task6.Models.ViewModels;
using Task6.Services;

namespace Task6.Controllers
{

    [Area("IdentifyUser")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        [BindProperty]
        public SearchModel SearchModel { get; set; }
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        [Authorize]
        public  IActionResult Index()
        {          
            return View();                       
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [Authorize]
        public IActionResult MessageDetails(string query, DateTime startDate, DateTime endDate, int pageSize, int pageNumber=1)
        {      
            if (ModelState.IsValid)
            {
                MessagesListViewModel messagesVM = new MessagesListViewModel()
                {
                    Messages = new List<MessagesViewModel>()
                };
                GMailService.User = User.Identity.Name;
                using (var gMailService = new GMailService())
                {
                    messagesVM.Messages = gMailService.ListMessages(
                        "subject:" + query +
                        " after:" + startDate.ToShortDateString() +
                        " before:" +endDate.ToShortDateString());
                }
                var count = messagesVM.Messages.Count;
                messagesVM.Messages = messagesVM.Messages.OrderByDescending(p => p.Title)
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize).ToList();

                messagesVM.PagingInfo = new PagingInfo
                {
                    CurrentPage = pageNumber,
                    ItemsPerPage = pageSize,
                    TotalItem = count,
                    urlParam = "/IdentifyUser/Home/MessageDetails?productPage=:"
                };
                return Json(messagesVM);
                //return PartialView("MessageDetails", messagesVM);
            }
            return RedirectToAction("Index");
        }

    }
}
